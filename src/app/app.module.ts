import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';

import { HeaderComponent } from './header/header.component';
import { NavbarComponent } from './navbar/navbar.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { ArticlesComponent } from './articles/articles.component';
import { MedicinesComponent } from './medicines/medicines.component';
import { ShopsComponent } from './shops/shops.component';
import { ContactComponent } from './contact/contact.component';
import { CarouselComponent } from './home/carousel/carousel.component';
import { MostPopularArticlesComponent } from './home/most-popular-articles/most-popular-articles.component';
import { RecentlyViewedComponent } from './home/recently-viewed/recently-viewed.component';
import { NewProductsComponent } from './home/new-products/new-products.component';
import { AdvertisementComponent } from './home/advertisement/advertisement.component';
import { SubscriptionComponent } from './home/subscription/subscription.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NavbarComponent,
    HomeComponent,
    FooterComponent,
    ArticlesComponent,
    MedicinesComponent,
    ShopsComponent,
    ContactComponent,
    CarouselComponent,
    MostPopularArticlesComponent,
    RecentlyViewedComponent,
    NewProductsComponent,
    AdvertisementComponent,
    SubscriptionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    NgbModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
